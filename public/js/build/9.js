webpackJsonp([9],{

/***/ 296:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(460)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/pages/website/home.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-44b9bc12", Component.options)
  } else {
    hotAPI.reload("data-v-44b9bc12", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 328:
/***/ (function(module, exports) {

module.exports = "/images/statics/public/Home-4 _ Dilorn_files/1.png?db9967c6";

/***/ }),

/***/ 329:
/***/ (function(module, exports) {

module.exports = "/images/statics/public/Home-4 _ Dilorn_files/2.png?6ba171d0";

/***/ }),

/***/ 336:
/***/ (function(module, exports) {

module.exports = "/images/statics/public/Home-4 _ Dilorn_files/3.png?d75678bf";

/***/ }),

/***/ 337:
/***/ (function(module, exports) {

module.exports = "/images/statics/public/Home-4 _ Dilorn_files/4.png?4cad443a";

/***/ }),

/***/ 338:
/***/ (function(module, exports) {

module.exports = "/images/statics/public/Home-4 _ Dilorn_files/5.png?9e1285f2";

/***/ }),

/***/ 339:
/***/ (function(module, exports) {

module.exports = "/images/statics/public/Home-4 _ Dilorn_files/6.png?272df0ec";

/***/ }),

/***/ 460:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Header"),
      _vm._v(" "),
      _c("div", { staticClass: "intro-area intro-area-4" }, [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "intro-content" }, [
          _c("div", { staticClass: "slider-content text-center" }, [
            _c("div", { staticClass: "container" }, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-md-12 col-sm-12 col-xs-12" }, [
                  _vm._m(1),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "layer-3 wow fadeInUp",
                      staticStyle: {
                        visibility: "visible",
                        "animation-delay": "0.7s",
                        "animation-name": "fadeInUp"
                      },
                      attrs: { "data-wow-delay": "0.7s" }
                    },
                    [
                      _c(
                        "a",
                        {
                          staticClass: "ready-btn left-btn",
                          attrs: { href: "javascript:void(0)" },
                          on: {
                            click: function($event) {
                              return _vm.webPage("/register")
                            }
                          }
                        },
                        [_vm._v("Try for Free")]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _vm._m(2)
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _vm._m(3),
      _vm._v(" "),
      _vm._m(4),
      _vm._v(" "),
      _vm._m(5),
      _vm._v(" "),
      _vm._m(6),
      _vm._v(" "),
      _c("Footer")
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bg-wrapper" }, [
      _c("img", {
        attrs: {
          src: __webpack_require__(461),
          alt: ""
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "layer-1 wow fadeInUp",
        staticStyle: {
          visibility: "visible",
          "animation-delay": "0.3s",
          "animation-name": "fadeInUp"
        },
        attrs: { "data-wow-delay": "0.3s" }
      },
      [
        _c("h2", { staticClass: "title2" }, [
          _vm._v("Lightweight work managent tool for teams that move fast.")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "slide-images-inner wow fadeInRight",
        staticStyle: {
          visibility: "visible",
          "animation-delay": "0.5s",
          "animation-name": "fadeInRight"
        },
        attrs: { "data-wow-delay": "0.5s" }
      },
      [
        _c("div", { staticClass: "slide-images" }, [
          _c("img", {
            attrs: {
              src: __webpack_require__(462),
              alt: ""
            }
          })
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "brand-area bg-color brand-area-3" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12 col-sm-12 col-xs-12" }, [
            _c("div", { staticClass: "brand-content" }, [
              _c(
                "div",
                {
                  staticClass:
                    "brand-items brand-carousel owl-carousel owl-theme owl-loaded"
                },
                [
                  _c("div", { staticClass: "owl-stage-outer" }, [
                    _c(
                      "div",
                      {
                        staticClass: "owl-stage",
                        staticStyle: {
                          transform: "translate3d(-1190px, 0px, 0px)",
                          transition: "all 0s ease 0s",
                          width: "3966.66px"
                        }
                      },
                      [
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(336),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(337),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(338),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(339),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(328),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(329),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item active",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(328),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item active",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(329),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item active",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(336),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item active",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(337),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item active",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(338),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item active",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(339),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item active",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(328),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(329),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(328),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(329),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(336),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(337),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(338),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        ),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: {
                              width: "148.333px",
                              "margin-right": "50px"
                            }
                          },
                          [
                            _c("div", { staticClass: "single-brand-item" }, [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(339),
                                    alt: ""
                                  }
                                })
                              ])
                            ])
                          ]
                        )
                      ]
                    )
                  ]),
                  _c("div", { staticClass: "owl-controls" }, [
                    _c("div", { staticClass: "owl-nav" }, [
                      _c(
                        "div",
                        {
                          staticClass: "owl-prev",
                          staticStyle: { display: "none" }
                        },
                        [_vm._v("prev")]
                      ),
                      _c(
                        "div",
                        {
                          staticClass: "owl-next",
                          staticStyle: { display: "none" }
                        },
                        [_vm._v("next")]
                      )
                    ]),
                    _c("div", { staticClass: "owl-dots" }, [
                      _c("div", { staticClass: "owl-dot active" }, [
                        _c("span")
                      ]),
                      _c("div", { staticClass: "owl-dot" }, [_c("span")])
                    ])
                  ])
                ]
              )
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "about-area fix area-padding", attrs: { id: "pmngt" } },
      [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6 col-sm-6 col-xs-12" }, [
              _c("div", { staticClass: "about-content" }, [
                _c("div", { staticClass: "about-images" }, [
                  _c("img", {
                    attrs: {
                      src: __webpack_require__(463),
                      alt: ""
                    }
                  })
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6 col-sm-6 col-xs-12" }, [
              _c("div", { staticClass: "about-text" }, [
                _c("h3", [_vm._v("Project Management")]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "Our Board is an incredible visual tool that provides an overview of the current work status, designed to simplify team communication and efficiency in performing tasks."
                  )
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "In the board, the workflow is displayed as a sequence of steps representing tasks from the beginning to the end of the project. Viewing the workflow allows a team to map all work steps in the Board columns and track work tasks as they move through them."
                  )
                ])
              ])
            ])
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "feature-area-3 bg-color area-padding",
        attrs: { id: "tmngt" }
      },
      [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6 col-sm-6 col-xs-12" }, [
              _c(
                "div",
                {
                  staticClass: "feature-inner wow fadeInLeft",
                  staticStyle: {
                    visibility: "visible",
                    "animation-delay": "0.3s",
                    "animation-name": "fadeInLeft"
                  },
                  attrs: { "data-wow-delay": "0.3s" }
                },
                [
                  _c("div", { staticClass: "feature-text" }, [
                    _c("h3", [_vm._v("Task Management")]),
                    _vm._v(" "),
                    _c("p", [
                      _vm._v(
                        "Using a four quadrant to-do list, tasks are organised by priority so that teams can see immediately what they need to do and which tasks to prioritize."
                      )
                    ]),
                    _vm._v(" "),
                    _c("h4", [_vm._v("Different views for your tasks")]),
                    _vm._v(" "),
                    _c("ul", [
                      _c("li", [
                        _c("a", { attrs: { href: "#" } }, [
                          _c("span", [
                            _c("i", { staticClass: "flaticon-app" })
                          ]),
                          _vm._v("Board view")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("a", { attrs: { href: "#" } }, [
                          _c("span", [
                            _c("i", { staticClass: "flaticon-file" })
                          ]),
                          _vm._v("List view")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("a", { attrs: { href: "#" } }, [
                          _c("span", [
                            _c("i", { staticClass: "flaticon-calendar-1" })
                          ]),
                          _vm._v("Calender view")
                        ])
                      ])
                    ])
                  ])
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6 col-sm-6 col-xs-12" }, [
              _c(
                "div",
                {
                  staticClass: "feature-image wow fadeInRight",
                  staticStyle: {
                    visibility: "visible",
                    "animation-delay": "0.3s",
                    "animation-name": "fadeInRight"
                  },
                  attrs: { "data-wow-delay": "0.3s" }
                },
                [
                  _c("img", {
                    attrs: {
                      src: __webpack_require__(464),
                      alt: ""
                    }
                  })
                ]
              )
            ])
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "services-area bg-color area-padding-2",
        attrs: { id: "features" }
      },
      [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12 col-sm-12 col-xs-12" }, [
              _c("div", { staticClass: "section-headline text-center" }, [
                _c("h3", [_vm._v("Features")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12 col-sm-12 col-xs-12" }, [
              _c("div", { staticClass: "row agency-services" }, [
                _c("div", { staticClass: "col-md-4 col-sm-6 col-xs-12" }, [
                  _c("div", { staticClass: "single-service" }, [
                    _c("div", { staticClass: "single-service-inner" }, [
                      _c("div", { staticClass: "service-content" }, [
                        _c("div", { staticClass: "service-icon" }, [
                          _c("a", { attrs: { href: "javascript:void(0)" } }, [
                            _c("i", { staticClass: "flaticon-price-tag" })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "service-text" }, [
                          _c("h4", [_vm._v("Task Labels")]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v("Classify tasks with color-coded labels.")
                          ])
                        ])
                      ])
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-4 col-sm-6 col-xs-12" }, [
                  _c("div", { staticClass: "single-service" }, [
                    _c("div", { staticClass: "single-service-inner" }, [
                      _c("div", { staticClass: "service-content" }, [
                        _c("div", { staticClass: "service-icon" }, [
                          _c("a", { attrs: { href: "javascript:void(0)" } }, [
                            _c("i", { staticClass: "flaticon-picture" })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "service-text" }, [
                          _c("h4", [_vm._v("Personalized backgrounds")]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(
                              "Choose the background image for your project board."
                            )
                          ])
                        ])
                      ])
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-4 col-sm-6 col-xs-12" }, [
                  _c("div", { staticClass: "single-service" }, [
                    _c("div", { staticClass: "single-service-inner" }, [
                      _c("div", { staticClass: "service-content" }, [
                        _c("div", { staticClass: "service-icon" }, [
                          _c("a", { attrs: { href: "javascript:void(0)" } }, [
                            _c("i", { staticClass: "flaticon-exit" })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "service-text" }, [
                          _c("h4", [_vm._v("Transferable projects")]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v("Transfer projects to other user accounts.")
                          ])
                        ])
                      ])
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-4 col-sm-6 col-xs-12" }, [
                  _c("div", { staticClass: "single-service" }, [
                    _c("div", { staticClass: "single-service-inner" }, [
                      _c("div", { staticClass: "service-content" }, [
                        _c("div", { staticClass: "service-icon" }, [
                          _c("a", { attrs: { href: "javascript:void(0)" } }, [
                            _c("i", { staticClass: "flaticon-archive-1" })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "service-text" }, [
                          _c("h4", [_vm._v("Archived tasks")]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v("Archive tasks to declutter your boards.")
                          ])
                        ])
                      ])
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-4 col-sm-6 col-xs-12" }, [
                  _c("div", { staticClass: "single-service" }, [
                    _c("div", { staticClass: "single-service-inner" }, [
                      _c("div", { staticClass: "service-content" }, [
                        _c("div", { staticClass: "service-icon" }, [
                          _c("a", { attrs: { href: "javascript:void(0)" } }, [
                            _c("i", { staticClass: "flaticon-controls-4" })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "service-text" }, [
                          _c("h4", [_vm._v("Powerful filters")]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(
                              "User powerful filters to quickly locate tasks on the project board."
                            )
                          ])
                        ])
                      ])
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-4 col-sm-6 col-xs-12" }, [
                  _c("div", { staticClass: "single-service" }, [
                    _c("div", { staticClass: "single-service-inner" }, [
                      _c("div", { staticClass: "service-content" }, [
                        _c("div", { staticClass: "service-icon" }, [
                          _c("a", { attrs: { href: "javascript:void(0)" } }, [
                            _c("i", { staticClass: "flaticon-document" })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "service-text" }, [
                          _c("h4", [_vm._v("Report generation")]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(
                              "Generate reports and have them sent to your inbox."
                            )
                          ])
                        ])
                      ])
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
      ]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-44b9bc12", module.exports)
  }
}

/***/ }),

/***/ 461:
/***/ (function(module, exports) {

module.exports = "/images/statics/public/Home-4 _ Dilorn_files/bgp3.png?95e7bf31";

/***/ }),

/***/ 462:
/***/ (function(module, exports) {

module.exports = "/images/statics/public/Home-4 _ Dilorn_files/s3.png?b80acd84";

/***/ }),

/***/ 463:
/***/ (function(module, exports) {

module.exports = "/images/statics/public/Home-4 _ Dilorn_files/ab3.png?63686033";

/***/ }),

/***/ 464:
/***/ (function(module, exports) {

module.exports = "/images/statics/public/Home-4 _ Dilorn_files/ab5.png?b128220f";

/***/ })

});
webpackJsonp([0],{

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            loginType: 'login',
            message: '',
            messageType: 'success',

            formLogin: {
                username: '',
                email: '',
                userpass: '',
                userpass2: ''
            },
            ruleLogin: {},

            systemConfig: $A.jsonParse($A.storage("systemSetting"), {
                logo: '',
                github: '',
                reg: ''
            }),

            fromUrl: ''
        };
    },
    mounted: function mounted() {
        var routeName = this.$route.name;

        if (routeName === 'register') {
            this.loginType = 'reg';
        }
    },

    methods: {
        onLogin: function onLogin() {
            var _this = this;

            // this.$refs.login.validate((valid) => {
            var valid = true;
            if (valid) {
                this.loadIng++;
                $A.ajax({
                    url: $A.apiUrl('users/login?type=' + this.loginType),
                    data: this.formLogin,
                    complete: function complete() {
                        _this.loadIng--;
                    },
                    success: function success(res) {
                        if (res.ret === 1) {
                            $A.storage("userInfo", res.data);
                            $A.setToken(res.data.token);
                            $A.triggerUserInfoListener(res.data);
                            //
                            //this.loadIng--;
                            //this.$refs.login.resetFields();
                            //this.$Message.success(this.$L('login successful'));
                            _this.message = 'Login successful.';
                            _this.messageType = 'success';

                            if (_this.fromUrl) {
                                console.log('from url');
                                window.location.replace(_this.fromUrl);
                            } else {

                                //this.goForward({path: '/todo'}, true);
                                window.location.replace('/todo');
                                //window.location.href = 'http://127.0.0.1:8000/todo';
                            }
                        } else {
                            _this.message = res.msg;
                            _this.messageType = 'danger';
                            /* this.$Modal.error({
                                title: this.$L("温馨提示"),
                                content: res.msg
                            }); */
                        }
                    }
                });
            }
            //})
        }
    }
});

/***/ }),

/***/ 466:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Header"),
      _vm._v(" "),
      _c("div", { staticClass: "login-area area-padding" }, [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12 col-sm-12 col-xs-12" }, [
              _c("div", { staticClass: "login-page" }, [
                _vm._m(0),
                _vm._v(" "),
                _c("div", { staticClass: "login-form" }, [
                  _c("h4", { staticClass: "login-title" }, [
                    _vm._v(
                      _vm._s(
                        _vm.$L(_vm.loginType == "reg" ? "Register" : "Login")
                      )
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _c(
                      "form",
                      {
                        ref: "login",
                        staticClass: "log-form",
                        attrs: { model: _vm.formLogin }
                      },
                      [
                        _vm.message != ""
                          ? _c(
                              "div",
                              { staticClass: "col-md-12 col-sm-12 col-xs-12" },
                              [
                                _c(
                                  "div",
                                  {
                                    class: "alert alert-" + _vm.messageType,
                                    attrs: { role: "alert" }
                                  },
                                  [_vm._v(_vm._s(_vm.message))]
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-md-12 col-sm-12 col-xs-12" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.formLogin.username,
                                  expression: "formLogin.username"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                type: "text",
                                id: "name",
                                prop: "username",
                                placeholder: "Username",
                                required: "",
                                "data-error": "Please enter your username"
                              },
                              domProps: { value: _vm.formLogin.username },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.formLogin,
                                    "username",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ]
                        ),
                        _vm._v(" "),
                        _vm.loginType == "reg"
                          ? _c(
                              "div",
                              { staticClass: "col-md-12 col-sm-12 col-xs-12" },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.formLogin.email,
                                      expression: "formLogin.email"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "email",
                                    prop: "email",
                                    placeholder: "Email",
                                    required: "",
                                    "data-error": "Please enter your email"
                                  },
                                  domProps: { value: _vm.formLogin.email },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.formLogin,
                                        "email",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-md-12 col-sm-12 col-xs-12" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.formLogin.userpass,
                                  expression: "formLogin.userpass"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                type: "password",
                                prop: "userpass",
                                placeholder: "Password",
                                required: "",
                                "data-error": "Please enter your password"
                              },
                              domProps: { value: _vm.formLogin.userpass },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.formLogin,
                                    "userpass",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ]
                        ),
                        _vm._v(" "),
                        _vm.loginType == "reg"
                          ? _c(
                              "div",
                              { staticClass: "col-md-12 col-sm-12 col-xs-12" },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.formLogin.userpass2,
                                      expression: "formLogin.userpass2"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "password",
                                    prop: "userpass2",
                                    placeholder: "Confirm Password",
                                    required: "",
                                    "data-error": "Please confirm your password"
                                  },
                                  domProps: { value: _vm.formLogin.userpass2 },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.formLogin,
                                        "userpass2",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.loginType == "login"
                          ? _c(
                              "div",
                              { staticClass: "col-md-12 col-sm-12 col-xs-12" },
                              [_vm._m(1)]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-md-12 col-sm-12 col-xs-12" },
                          [
                            _c(
                              "button",
                              {
                                staticClass: "slide-btn login-btn",
                                attrs: { type: "button", id: "submit" },
                                on: { click: _vm.onLogin }
                              },
                              [
                                _vm._v(
                                  _vm._s(
                                    _vm.$L(
                                      _vm.loginType == "reg"
                                        ? "Register"
                                        : "Login"
                                    )
                                  )
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("div", {
                              staticClass: "h3 text-center hidden",
                              attrs: { id: "msgSubmit" }
                            }),
                            _vm._v(" "),
                            _c("div", { staticClass: "clearfix" })
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-md-12 col-sm-12 col-xs-12" },
                          [
                            _c("div", { staticClass: "clear" }),
                            _vm._v(" "),
                            _c("div", [
                              _vm.loginType == "reg"
                                ? _c("div", { staticClass: "acc-not" }, [
                                    _vm._v("Already have an account  "),
                                    _c(
                                      "a",
                                      {
                                        attrs: { href: "javascript:void(0)" },
                                        on: {
                                          click: function($event) {
                                            _vm.loginType = "login"
                                          }
                                        }
                                      },
                                      [_vm._v(" Log in")]
                                    )
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.loginType == "login"
                                ? _c("div", { staticClass: "acc-not" }, [
                                    _vm._v("Don't have an account  "),
                                    _c(
                                      "a",
                                      {
                                        attrs: { href: "javascript:void(0)" },
                                        on: {
                                          click: function($event) {
                                            _vm.loginType = "reg"
                                          }
                                        }
                                      },
                                      [_vm._v(" Sign up")]
                                    )
                                  ])
                                : _vm._e()
                            ])
                          ]
                        )
                      ]
                    )
                  ])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("Footer")
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "login-image" }, [
      _c("div", { staticClass: "log-inner" }, [
        _c("img", {
          attrs: {
            src: __webpack_require__(467),
            alt: ""
          }
        })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "check-group flexbox" }, [
      _c("label", { staticClass: "check-box" }, [
        _c("input", {
          staticClass: "check-box-input",
          attrs: { type: "checkbox", checked: "" }
        }),
        _vm._v(" "),
        _c("span", { staticClass: "remember-text" }, [_vm._v("Remember me")])
      ]),
      _vm._v(" "),
      _c(
        "a",
        {
          staticClass: "text-muted",
          attrs: {
            href:
              "http://rockstheme.com/rocks/dilorn-preview/dilorn/login.html#"
          }
        },
        [_vm._v("Forgot password?")]
      )
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7062acc6", module.exports)
  }
}

/***/ }),

/***/ 467:
/***/ (function(module, exports) {

module.exports = "/images/statics/public/Home-4 _ Dilorn_files/ab2.jpg?1214aa53";

/***/ }),

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(465)
/* template */
var __vue_template__ = __webpack_require__(466)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/pages/website/login.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7062acc6", Component.options)
  } else {
    hotAPI.reload("data-v-7062acc6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});
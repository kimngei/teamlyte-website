<!DOCTYPE html>
<!-- saved from url=(0060)http://rockstheme.com/rocks/dilorn-preview/dilorn/login.html -->
<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths" lang="en" style=""><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Login | Teamlyte</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- favicon -->		
		<link rel="shortcut icon" type="image/x-icon" href="http://rockstheme.com/rocks/dilorn-preview/dilorn/img/logo/favicon.ico">

		<!-- all css here -->
        @include('website/styles')

		<!-- modernizr css -->
		<script src="./Login _ Dilorn_files/modernizr-2.8.3.min.js.download"></script>
	</head>
		<body data-new-gr-c-s-check-loaded="14.987.0" data-gr-ext-installed="">

		<!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

        
        <div id="app"></div>
       
        <!-- all js here -->
        @include('website/scripts')

        <a id="scrollUp" href="#top" style="position: fixed; z-index: 2147483647; display: block;"><i class="icon icon-chevron-up"></i></a>
        
        <script src="{{ mix('js/app.js') }}?v={{ $version }}"></script>
	
</body></html>
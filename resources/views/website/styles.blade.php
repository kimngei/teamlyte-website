        <!-- bootstrap v3.3.6 css -->
        <link rel="stylesheet" href="{{ asset('Home-4 _ Dilorn_files/bootstrap.min.css')}}">
		<!-- owl.carousel css -->
		<link rel="stylesheet" href="./Home-4 _ Dilorn_files/owl.carousel.css">
		<link rel="stylesheet" href="./Home-4 _ Dilorn_files/owl.transitions.css">
       <!-- Animate css -->
        <link rel="stylesheet" href="./Home-4 _ Dilorn_files/animate.css">
        <!-- Venobox css -->
        <link rel="stylesheet" href="./Home-4 _ Dilorn_files/venobox.css">
        <!-- meanmenu css -->
        <link rel="stylesheet" href="./Home-4 _ Dilorn_files/meanmenu.min.css">
		<!-- font-awesome css -->
		<link rel="stylesheet" href="./Home-4 _ Dilorn_files/font-awesome.min.css">
		<link rel="stylesheet" href="./Home-4 _ Dilorn_files/icon.css">
		<link rel="stylesheet" href="./Home-4 _ Dilorn_files/flaticon.css">
		<!-- magnific css -->
        <link rel="stylesheet" href="./Home-4 _ Dilorn_files/magnific.min.css">
		<!-- style css -->
		<link rel="stylesheet" href="./Home-4 _ Dilorn_files/style.css">
		<!-- responsive css -->
		<link rel="stylesheet" href="./Home-4 _ Dilorn_files/responsive.css">
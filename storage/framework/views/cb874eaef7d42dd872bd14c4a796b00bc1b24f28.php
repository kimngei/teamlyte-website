<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo e(config('app.name', 'WebPage')); ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo e(mix('css/app.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(asset('css/iview.css')); ?>">
    <script src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
    <script>
        window.csrfToken = { csrfToken : "<?php echo e(csrf_token()); ?>" };
        window.webSocketConfig = { URL: "<?php echo e(env('LARAVELS_PROXY_URL')); ?>" };
    </script>
</head>
<body>


<div id="app">
    <div class="app-view-loading">
        <div>
            <div>PAGE LOADING</div>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</div>

<script src="<?php echo e(mix('js/app.js')); ?>?v=<?php echo e($version); ?>"></script>

</body>
</html>

<?php echo $__env->make('ie', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/520142.cloudwaysapps.com/hrhcnbxqtf/public_html/resources/views/main.blade.php ENDPATH**/ ?>